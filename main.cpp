#include <structures.hpp>
#include <reader.hpp>

// just to show progress
#define BW 60
#define BS "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
void printProgress(double progress) {
    int val = (int) progress * 100;
    int lpad = (int) progress * BW;
    int rpad = BW - lpad;
    printf("\r%3d%% [%.*s%*s]", val, lpad, BS, rpad, "");
    fflush(stdout);
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
        std::cerr << "Usage: " << "./ts_parser INPUT AUDIO_OUT VIDEO_OUT" << std::endl;
        std::cerr << "Example: " << "./ts_parser input.ts audio.aac video.avc1" << std::endl;
        std::cerr << "Example: " << "./ts_parser input.ts audio.aac video.h264" << std::endl;

        return 1;
    }

    std::time_t cs = clock();

    // cv for communication with reader
    std::mutex mutex;
    std::condition_variable cv;

    // demuxer module
    Reader reader;
    reader.mutex = &mutex;
    reader.cv = &cv;

    reader.setFiles(argv[1], argv[2], argv[3]);
    reader.start();
    std::cout << "Started reading..." << std::endl;

    // wait until reader is started with fail (bad input)
    // or until PAT and PMTs are collected
    std::unique_lock<std::mutex> lock(mutex);
    cv.wait(lock, [&] { return reader.isReady() || reader.isFinished(); });

    // handle bad input
    if(!reader.isReady() && reader.isFinished()) {
        std::cout << "Sync byte not found !" << std::endl;
        reader.stop();
        return -1;
    }
    else std::cout << "PAT and PMTs are received..." << std::endl;

    lock.unlock();

    // get progress report
    do {
        double progress = reader.getProgress();
        printProgress(progress);
    }
    while(!reader.isFinished());
    reader.stop();

    // success
    printProgress(1);
    std::cout << std::endl;
    std::cout << "Demuxing finished in " << (clock() - cs) / (float) CLOCKS_PER_SEC << " seconds" << std::endl << std::endl;

    return 0;
}
