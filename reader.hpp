#ifndef READER_HPP
#define READER_HPP

/*
 * reader.hpp
 *
 * reads a local file and demuxes streams into buffers
 * runs within a seperate thread
 *
 */

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <structures.hpp>
#include <cstring>
#include <byteswap.h>
#include <endian.h>
#include <unordered_map>
#include <set>
#include <iostream>

#define TS_PACKET_SIZE 188

#define PSI_TID_PAT 0x00
#define PSI_TID_CAT 0x01
#define PSI_TID_PMT 0x02
#define PSI_TID_DSC 0x03

// only AAC and H264 SID types are supported for now
#define PSI_SID_AUDIO 15
#define PSI_SID_VIDEO 27

class Reader
{
public:
    // only two types are supported for now, others are not parsed
    enum PayloadType {
        PSI,
        PES
    };

    Reader() :  m_pos(0), m_isEnd(0), m_isReady(0) { }
    ~Reader() {}

    void setFiles(const std::string & input, const std::string & out_audio, const std::string & out_video) {
        m_file.open(input, std::ifstream::binary);
        m_file_audio.open(out_audio, std::ofstream::binary);
        m_file_video.open(out_video, std::ofstream::binary);
    }

    // start reading
    void start();

    // stop reading interrupt
    void stop();

    // all different stream ids
    std::set<uint16_t> stream_ids;

    // information about progress
    bool isReady() const { return m_isReady; }
    bool isFinished() const { return m_isEnd; }
    double getProgress() const { if((double) m_file_size > 0) return (m_pos / (double) m_file_size); else return 0; }
    uint64_t getFileSize() const { return (uint64_t) m_file_size; }

    // condition var to signal progress
    std::condition_variable * cv;
    std::mutex * mutex;

private:
    // IO related
    std::ifstream m_file;
    std::ofstream m_file_audio;
    std::ofstream m_file_video;
    std::streamsize m_file_size;

    // current position in bytes
    uint32_t m_pos;

    /*
     *
     * Serialization method
     *
     */
    template<typename D, typename S>
    void m_serialize(D & dest, S & src);

    // program number <-> program id table
    std::unordered_map<uint16_t, uint16_t> pat_hash;

    // program id <-> stream id table
    std::unordered_multimap<uint16_t, uint16_t> pmt_hash;

    // stream id <-> stream type table
    std::unordered_map<uint16_t, uint16_t> stream_hash;

    // stream type <-> custom id table
    std::unordered_map<uint16_t, uint16_t> stream_cid_hash;

    // early packets that has PAT and PMTs
    std::queue<std::vector<char>> m_header_queue;

    // thread
    std::unique_ptr<std::thread> m_thread;

    bool m_isEnd;
    bool m_isReady;
};

#endif // READER_HPP
