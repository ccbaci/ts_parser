#ifndef STRUCTURES_HPP
#define STRUCTURES_HPP

/*
 * structures.hpp
 *
 * contains bit fields for representation of a 188-byte ts packet
 * according to http://www.img.lx.it.pt/~fp/cav/Additional_material/MPEG2_overview.pdf, page 36 figure 7-1
 * and https://en.wikipedia.org/wiki/MPEG_transport_stream#Packet
 * and https://en.wikipedia.org/wiki/Program-specific_information#PSI_structure
 *
 * endianness checks are done for fast serialization
 *
 */

#define _FLAG_      1
#define _SYNC_      'G'

#include <stdint.h>
#include <endian.h>

/*
 *
 * TS header - until adaptation - is 32 bits
 *
 */
struct TransportStreamPacketHeader {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t continuity_counter                  : 4;
    uint8_t adaptation_field_control            : 2;
    uint8_t scrambling_control                  : 2;

    uint16_t pid                                : 13;
    uint8_t transport_priority                  : _FLAG_;
    uint8_t start_indicator                     : _FLAG_;
    uint8_t transport_error_indicator           : _FLAG_;

    uint8_t sync_byte                           : 8;
#else
    uint8_t sync_byte                           : 8;

    uint8_t transport_error_indicator           : _FLAG_;
    uint8_t start_indicator                     : _FLAG_;
    uint8_t transport_priority                  : _FLAG_;
    uint16_t pid                                : 13;

    uint8_t scrambling_control                  : 2;
    uint8_t adaptation_field_control            : 2;
    uint8_t continuity_counter                  : 4;
#endif
} __attribute__((packed));


/*
 *
 * Controlled with TransportStreamPacketHeader::adaptation_field_control
 * Adaptation bit field is 16 bits
 *
 */
struct AdaptationField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t extension_flag                      : _FLAG_;
    uint8_t transport_private_data_flag         : _FLAG_;
    uint8_t splicing_point_flag                 : _FLAG_;
    uint8_t opcr_flag                           : _FLAG_;
    uint8_t pcr_flag                            : _FLAG_;
    uint8_t elem_stream_priority_indicator      : _FLAG_;
    uint8_t random_access_indicator             : _FLAG_;
    uint8_t discontinuity_indicator             : _FLAG_;

    uint8_t adaptation_field_length             : 8;
#else
    uint8_t adaptation_field_length             : 8;

    uint8_t discontinuity_indicator             : _FLAG_;
    uint8_t random_access_indicator             : _FLAG_;
    uint8_t elem_stream_priority_indicator      : _FLAG_;
    uint8_t pcr_flag                            : _FLAG_;
    uint8_t opcr_flag                           : _FLAG_;
    uint8_t splicing_point_flag                 : _FLAG_;
    uint8_t transport_private_data_flag         : _FLAG_;
    uint8_t extension_flag                      : _FLAG_;
#endif
} __attribute__((packed));


/*
 *
 *
 * Program clock reference,
 * stored as 33 bits base, 6 bits reserved, 9 bits extension. total 48 bits
 * The value is calculated as base * 300 + extension.
 * Controlled by AdaptationField::pcr_flag and AdaptationField::opcr_flag
 *
 *
 */
struct ProgramClockReference {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint16_t extension                          : 9;
    uint8_t reserved                            : 6;
    uint64_t base                               : 33;
#else
    uint64_t base                               : 33;
    uint8_t reserved                            : 6;
    uint16_t extension                          : 9;
#endif
} __attribute__((packed));


/*
 *
 *
 * Splice countdown.
 * Controlled by AdaptationField::splicing_point_flag
 * May be negative. 8 bits signed.
 *
 */
struct SpliceCountdown {
    int8_t splice_countdown                     : 8;
};


/*
 *
 *
 * Transport private data length information.
 * 8 bits
 *
 */
struct TransportPrivate {
    uint8_t length                               : 8;
};

/*
 *
 *
 * Adaptation extension.
 * Controlled by AdaptationField::extension_flag
 * 16 bits
 *
 *
 */
struct AdaptationExtensionField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t adaptation_reserved                 : 5;
    uint8_t seamless_splice_flag                : _FLAG_;
    uint8_t piecewise_rate_flag                 : _FLAG_;
    uint8_t ltw_flag                            : _FLAG_;

    uint8_t length                              : 8;
#else
    uint8_t length                              : 8;

    uint8_t ltw_flag                            : _FLAG_;
    uint8_t piecewise_rate_flag                 : _FLAG_;
    uint8_t seamless_splice_flag                : _FLAG_;
    uint8_t adaptation_reserved                 : 5;
#endif
} __attribute__((packed));


/*
 *
 *
 * Legal time window.
 * Controlled by AdaptationExtensionField::ltw_flag
 * 16 bits
 *
 *
 */
struct LegalTimeWindowField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint16_t ltw_offset                         : 15;
    uint8_t ltw_valid_flag                      : _FLAG_;
#else
    uint8_t ltw_valid_flag                      : _FLAG_;
    uint16_t ltw_offset                         : 15;
#endif
} __attribute__((packed));


/*
 *
 *
 * Piecewise rate.
 * Controlled by AdaptationExtensionField::piecewise_rate_flag
 * 24 bits
 *
 *
 */
struct PiecewiseField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint32_t piecewise_rate                     : 22;
    uint8_t piecewise_reserved                  : 2;
#else
    uint8_t piecewise_reserved                  : 2;
    uint32_t piecewise_rate                     : 22;
#endif
} __attribute__((packed));


/*
 *
 *
 * Seamless spice.
 * Controlled by AdaptationExtensionField::seamless_splice_flag
 * 40 bits
 *
 *
 */
struct SeamlessSpliceField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint64_t dts_next_access_unit               : 36;
    uint8_t splice_type                         : 4;
#else
    uint8_t splice_type                         : 4;
    uint64_t dts_next_access_unit               : 36;
#endif
};


/*
 *
 *
 * Payload data
 * PSI structures
 * TODO document
 *
 */

// 24 bits
struct TableHeader {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint16_t section_length                     : 10;
    uint8_t unused                              : 2;
    uint8_t reserved                            : 2;
    uint8_t priv_bit                            : _FLAG_;
    uint8_t section_syntax_indicator            : _FLAG_;

    uint8_t table_id                            : 8;
#else
    uint8_t table_id                            : 8;

    uint8_t section_syntax_indicator            : _FLAG_;
    uint8_t priv_bit                            : _FLAG_;
    uint8_t reserved                            : 2;
    uint8_t unused                              : 2;
    uint16_t section_length                     : 10;
#endif
} __attribute__((packed));

// 40 bits
struct TableSyntaxSection {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t last_section_number                 : 8;
    uint8_t section_number                      : 8;
    uint8_t current_next_indicator              : _FLAG_;
    uint8_t version_number                      : 5;
    uint8_t reserved                            : 2;
    uint16_t table_id_extension                 : 16;

#else
    uint16_t table_id_extension                 : 16;

    uint8_t reserved                            : 2;
    uint8_t version_number                      : 5;
    uint8_t current_next_indicator              : _FLAG_;

    uint8_t section_number                      : 8;
    uint8_t last_section_number                 : 8;

#endif

} __attribute__((packed));

// 16 bits
struct Descriptor {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint8_t length                              : 8 ;
    uint8_t tag                                 : 8 ;
#else
    uint8_t tag                                 : 8 ;
    uint8_t length                              : 8 ;
#endif


} __attribute__((packed));

struct PatField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint16_t program_map_id                     : 13;
    uint8_t reserved                            : 3;

    uint16_t program_num                        : 16;
#else
    uint16_t program_num                        : 16;

    uint8_t reserved                            : 3;
    uint16_t program_map_id                     : 13;
#endif
} __attribute__((packed));


struct PmtField {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint16_t program_info_length                : 10;
    uint8_t pil_unused                          : 2;
    uint8_t reserved_2                          : 4;

    uint16_t pcr_pid                            : 13;
    uint8_t reserved_1                          : 3;
#else
    uint8_t reserved_1                          : 3;
    uint16_t pcr_pid                            : 13;

    uint8_t reserved_2                          : 4;
    uint8_t pil_unused                          : 2;
    uint16_t program_info_length                : 10;


#endif
} __attribute__((packed));

struct Stream {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    uint16_t es_info_length                     : 10;
    uint8_t es_info_length_unused               : 2;
    uint8_t reserved_2                          : 4;

    uint16_t elementary_pid                     : 13;
    uint8_t reserved_1                          : 3;

    uint8_t stream_type                         : 8;
#else

    uint8_t stream_type                         : 8;

    uint8_t reserved_1                          : 3;
    uint16_t elementary_pid                     : 13;

    uint8_t reserved_2                          : 4;
    uint8_t es_info_length_unused               : 2;
    uint16_t es_info_length                     : 10;
#endif

} __attribute__((packed));



#endif // STRUCTURES_HPP
