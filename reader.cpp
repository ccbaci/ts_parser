﻿#include "reader.hpp"
#include <iostream>
/*
 *
 * Serialization method
 * data is always big endian in ts format
 * so we should swap bytes only if host is little endian
 * then serialize to destination bit field
 *
 */
template<typename D, typename S>
void Reader::m_serialize(D & dest, S & src) {
    switch (sizeof(D)) {
    case 1 : {
        std::memcpy(&dest, &src, sizeof(D)); }
        break;
    case 2 : {
        uint16_t t = 0; std::memcpy(&t, &src, sizeof(D));
        t = be16toh(t); std::memcpy(&dest, &t, sizeof(D)); }
        break;
    case 3 : {
        uint32_t t = 0; std::memcpy(&t, &src, sizeof(D));
        t = be32toh(t); t = t >> 8;
        std::memcpy(&dest, &t, sizeof(t)); }
        break;
    case 4 : {
        uint32_t t = 0; std::memcpy(&t, &src, sizeof(D));
        t = be32toh(t); std::memcpy(&dest, &t, sizeof(D)); }
        break;
    case 5 :
    case 6 :
    case 7 : {
        uint64_t t = 0; std::memcpy(&t, &src, sizeof(D));
        t = be64toh(t); t = t >> (8 * (8 - sizeof(D)));
        std::memcpy(&dest, &t, sizeof(D)); }
        break;
    case 8 : {
        uint64_t t = 0; std::memcpy(&t, &src, sizeof(D));
        t = be64toh(t); std::memcpy(&dest, &t, sizeof(D)); }
        break;
    default:
        break;
    }
}

void Reader::start()
{
    /*
     *
     * read continously
     * assuming first byte is sync byte.
     * TODO locate sync position
     *
     */

    TransportStreamPacketHeader tsph;
    AdaptationField af;
    ProgramClockReference pcr, opcr;
    SpliceCountdown sc;
    TransportPrivate tp;
    AdaptationExtensionField afe;
    TableHeader th;
    TableSyntaxSection ts;
    Descriptor d;
    PatField pat;
    PmtField pmt;
    Stream s;

    m_thread.reset(new std::thread( [&] {
        m_pos = 0;

        // file size
        m_file.ignore(std::numeric_limits<std::streamsize>::max());
        m_file_size = m_file.gcount();
        if(m_file_size == 0) { std::cout << "Bad input" << std::endl; m_isEnd = true; cv->notify_one(); return; }
        m_file.clear();

        while(1) {
            // abort signal
            if(m_isEnd) return;

            // end of file
            if(m_pos + TS_PACKET_SIZE >= m_file_size) { m_isEnd = true; cv->notify_one(); return; }

            std::lock_guard<std::mutex> lg(*mutex);

            // move to next ts packet pos
            m_file.seekg(m_file.beg + m_pos);

            // read whole packet into container
            std::vector<char> packet;
            packet.resize(TS_PACKET_SIZE);
            m_file.read(&packet[0], TS_PACKET_SIZE);

            bool pid_ex;
            int pos_cur, pos_payload;
            PayloadType payload_type;

            if(packet[0] != _SYNC_) { goto finish; }

            m_serialize(tsph, packet[0]);
            pos_cur = sizeof(tsph);
            pos_payload = pos_cur;

            // fetch the type of payload for parsing it
            pid_ex = false;
            payload_type = Reader::PSI;
            if(pat_hash.size()) {
                for(auto itr_pat = pat_hash.begin(); itr_pat != pat_hash.end(); ++itr_pat) {
                    // check if payload is program stream table data (PMT)
                    if(itr_pat->second == tsph.pid) {
                        payload_type = Reader::PSI;
                        pid_ex = true;
                        break;
                    }

                    // check if payload is elem. stream data (PES)
                    auto range = pmt_hash.equal_range(itr_pat->second);
                    if(range.first == range.second) continue;
                    for(auto itr_pmt = range.first; itr_pmt != range.second; ++itr_pmt) {
                        if(itr_pmt->second == tsph.pid) {
                            payload_type = Reader::PES;
                            break;
                        }
                    }
                    if(payload_type == Reader::PES) break;
                }
            }

            // unexpected pid
            if(!((payload_type == Reader::PSI && tsph.pid == 0) ||
                 pid_ex)) {

                // TODO error message
                // ...

            }

            // push packet and move to next one if PAT and PMTs are ready
            if(m_isReady) {
                auto it = stream_hash.find((uint16_t) tsph.pid);
                if(it == stream_hash.end()) { goto finish; }

                // push packet to the file
                if(it->second == PSI_SID_AUDIO) m_file_audio.write(&packet[0], TS_PACKET_SIZE);
                else if(it->second == PSI_SID_VIDEO) m_file_video.write(&packet[0], TS_PACKET_SIZE);

                goto finish;
            }

            // adaptation
            if(tsph.adaptation_field_control > 1) {
                // parse adaptation field
                m_serialize(af, packet[pos_cur]);
                pos_payload = pos_cur + 1 + af.adaptation_field_length;
                pos_cur += sizeof(af);

                // parse optional fields
                if(af.pcr_flag) {
                    m_serialize(pcr, packet[pos_cur]);
                    pos_cur += sizeof(pcr);

                    // parse
                    // ...
                }
                if(af.opcr_flag) {
                    m_serialize(opcr, packet[pos_cur]);
                    pos_cur += sizeof(opcr);

                    // parse
                    // ...
                }
                if(af.splicing_point_flag) {
                    m_serialize(sc, packet[pos_cur]);
                    pos_cur += sizeof(sc);

                    // parse
                    // ...
                }

                // private data
                if(af.transport_private_data_flag) {
                    m_serialize(tp, packet[pos_cur]);
                    pos_cur += sizeof(tp);

                    // there is private data
                    if(tp.length) {
                        pos_cur += tp.length;

                        // parse
                        // ...
                    }
                }

                // parse extension field
                if(af.extension_flag) {
                    m_serialize(afe, packet[pos_cur]);
                    pos_cur += afe.length;

                    // parse
                    // ...
                }
            }

            // payload
            if(tsph.adaptation_field_control % 2) {
                pos_cur = pos_payload;

                // incoming PSI
                if(payload_type == PayloadType::PSI) {

                    // pointer
                    if(tsph.start_indicator) {
                        uint8_t psi_pointer_field;
                        m_serialize(psi_pointer_field, packet[pos_cur]);
                        pos_cur += sizeof(psi_pointer_field);
                        if(psi_pointer_field) {
                            pos_cur += psi_pointer_field;
                        }
                    }

                    // table header
                    m_serialize(th, packet[pos_cur]);
                    pos_cur += sizeof(th);

                    if(th.section_syntax_indicator) {
                        if(th.section_length) {
                            uint16_t section_left = th.section_length;

                            m_serialize(ts, packet[pos_cur]);
                            pos_cur += sizeof(ts);
                            section_left -= sizeof(ts);

                            if(th.table_id == PSI_TID_PAT) {

                                // repeating PAT until end of section
                                do {
                                    if(pos_cur >= TS_PACKET_SIZE) goto finish;

                                    m_serialize(pat, packet[pos_cur]);
                                    pos_cur += sizeof(pat);
                                    section_left -= sizeof(pat);

                                    // insert or update program number key
                                    pat_hash.insert(std::make_pair((uint16_t) pat.program_num, (uint16_t) pat.program_map_id));
                                }
                                while(section_left > 4);
                            }
                            else if(th.table_id == PSI_TID_CAT) {

                                // parse
                                // ...

                            }
                            else if(th.table_id == PSI_TID_PMT) {
                                m_serialize(pmt, packet[pos_cur]);
                                pos_cur += sizeof(pmt);
                                section_left -= sizeof(pmt);

                                if(pmt.program_info_length) {
                                    pos_cur += pmt.program_info_length;
                                    section_left -= pmt.program_info_length;
                                }

                                // repeating streams until end of section
                                do {
                                    if(pos_cur >= TS_PACKET_SIZE) goto finish;

                                    m_serialize(s, packet[pos_cur]);
                                    pos_cur += sizeof(s);
                                    section_left -= sizeof(s);

                                    // insert streams into programs hash
                                    pmt_hash.insert(std::make_pair((uint16_t) tsph.pid, (uint16_t) s.elementary_pid));

                                    // save stream ids with their types
                                    stream_hash[s.elementary_pid] = s.stream_type;

                                    // save stream id to also set
                                    stream_ids.insert(s.elementary_pid);

                                    // also assign custom ids that may become handy
                                    if(s.stream_type == PSI_SID_AUDIO) stream_cid_hash[PSI_SID_AUDIO] = 0;
                                    else if(s.stream_type == PSI_SID_VIDEO) stream_cid_hash[PSI_SID_VIDEO] = 1;

                                    if(s.es_info_length) {
                                        pos_cur += s.es_info_length;
                                        section_left -= s.es_info_length;
                                    }

                                }
                                while(section_left > 4);
                            }
                            else if(th.table_id == PSI_TID_DSC) {
                                m_serialize(d, packet[pos_cur]);
                                pos_cur += sizeof(d);
                                section_left -= sizeof(d);

                                // parse description
                                // ...

                            }
                        }
                    }
                }
                else if(payload_type == Reader::PES){

                    // parse PES info
                    // ...

                }

                // support all payload types. DVB, NIT, etc.
                // else if ...
            }

            // save early (PAT and PMT) packets
            m_header_queue.push(packet);

            /*
            *
            * when first program table is receieved - PAT
            * and all stream tables for this program are received - PMTs
            * start to push different streams into different files
            *
            * TODO hack PMT before pushing so that media player doesnt cry for missing stream(s)
            * TODO support demux across multiple streams and across multiple programs
            *
            */

            if(pat_hash.size() && pmt_hash.size()) {

                while(m_header_queue.size()) {
                    std::vector<char> pkt_h = m_header_queue.front();
                    if(pkt_h.size()) {
                        m_file_audio.write(&pkt_h[0], TS_PACKET_SIZE);
                        m_file_video.write(&pkt_h[0], TS_PACKET_SIZE);
                        m_header_queue.pop();
                    }
                }

                m_isReady = true;
            }

finish:
            // move to next packet
            m_pos += TS_PACKET_SIZE;
            cv->notify_one();
        }

    }));
}

void Reader::stop()
{
    m_isEnd = true;
    if(NULL != m_thread) {
        m_thread->join();
        m_thread.reset();
    }
}
